<div class="column" id="<?php the_ID(); ?>">
	<a data-toggle="modal" data-target="#workModal">
		<?php the_post_thumbnail(); ?>
		<h3 class="light-text bold-text mt-10"><?php the_title();?></h3>
	</a>
</div>