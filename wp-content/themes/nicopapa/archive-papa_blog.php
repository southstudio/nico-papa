<?php get_header(); ?>

    <?php if ( have_posts() ) : ?>

        <div class="container-fluid blog-posts d-flex flex-column">
            <div class="row">
                <?php
                    while ( have_posts() ) : the_post();
                    $blogPostThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
                ?>
                    <div class="col-12 col-lg-4">
                        <div class="blog-post" onclick="location.href='<?php the_permalink(); ?>';">
                            <div class="post-thumbnail mb-20" style="background-image:url(<?php echo $blogPostThumbnail[0]?>);"></div>
                            <h3 class="light-text bold-text mb-10"><?php the_title(); ?></h3>
                            <p class="light-text mb-20"><?php echo get_the_date('d/m/Y'); ?></p>
                            <div class="excerpt light-text serif-text"><?php the_excerpt(); ?></div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>

            <?php

                $links = paginate_links(
                    array(
                        'prev_next'          => false,
                        'type'               => 'array'
                    )
                );
                    
                if ( $links ) ://If has pagination

                    $prev_posts_link = get_previous_posts_link( __( 'Anterior página' ) );
                    $next_posts_link = get_next_posts_link( __( 'Próxima página' ) );

                    echo '<div class="row papa-pagination">';
                        echo '<div class="col-12">';

                            if ( $prev_posts_link && !$next_posts_link){
                                echo '<div class="d-flex align-items-center justify-content-start">';
                            } elseif ( $next_posts_link && !$prev_posts_link ) {
                                echo '<div class="d-flex align-items-center justify-content-end">';
                            } else{
                                echo '<div class="d-flex align-items-center justify-content-between">';
                            }
                        
                                // get_previous_posts_link will return a string or void if no link is set.
                                if ( $prev_posts_link = get_previous_posts_link( __( '<i class="fas fa-long-arrow-alt-left light-text"></i> Anterior' ) ) ) :
                                    echo '<div class="prev-list-item">';
                                    echo $prev_posts_link;
                                    echo '</div>';
                                endif;
                            
                                // get_next_posts_link will return a string or void if no link is set.
                                if ( $next_posts_link = get_next_posts_link( __( 'Siguiente <i class="fas fa-long-arrow-alt-right light-text"></i>' ) ) ) :
                                    echo '<div class="next-list-item">';
                                    echo $next_posts_link;
                                    echo '</div>';
                                endif;

                            echo '</div>';

                        echo '</div>';

                    echo '</div>';

                endif;//End if has pagination

            ?>

        </div>

    <?php
        else ://If page doesn't have results
    ?>

        <div class="container no-results-alert">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="light-text bold-text mb-10">Ups!</h1>
                    <h2 class="light-text">There are no posts yet, but come back later!</h2>
                    <!--<a href="javascript:history.back()" class="light-text bold-text">Volver</a>-->
                </div>
            </div>
        </div>

	<?php endif; ?>

<?php get_footer(); ?>