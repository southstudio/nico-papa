<?php if (substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) ob_start("ob_gzhandler"); else ob_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="<?php echo get_bloginfo( 'name' );?>">
        <title><?php bloginfo('name'); ?><?php if (!is_front_page()) {?> | <?php wp_title(''); }?></title>
        <?php wp_head();?>
    </head>
    <body <?php body_class(); ?>>

        <?php $GLOBALS['global_url'] = 'http://nicopapafoto.com'; ?>

        <div id="pageLoading" class="page-loading">
            <?php echo '<img src="'.$GLOBALS['global_url'].'/wp-content/uploads/2019/06/NP_Loader_02.gif" class="loading">';?>
        </div>

        <div class="site d-flex flex-column">
        
            <header>
                <div class="container-fluid">

                    <div class="site-navbar d-flex align-items-center justify-content-between">
                        <div class="site-info d-flex align-items-stretch">
                            <!-- SITE LOGO -->
                            <?php if ( get_theme_mod( 'site_logo' ) ): ?>
                                <a class="site-logo" href="<?php echo esc_url( home_url( '/' )); ?>">
                                    <img class="img-fluid" src="<?php echo esc_attr(get_theme_mod( 'site_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                                </a>
                            <?php else : ?>
                                <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                            <?php endif; ?>

                            <!-- PRIMARY MENU -->
                            <?php
                                wp_nav_menu(array(
                                    'theme_location'    => 'primary',
                                    'container'       => 'div',
                                    'container_id'    => 'topNav',
                                    'container_class' => 'top-nav',
                                    'menu_class'      => 'menu-desktop d-flex flex-column',
                                ));
                            ?>
                        </div>

                        <div class="site-social-media">
                            <h2 class="light-text bold-text">Follow me on <?php if ( get_theme_mod( 'facebook' ) ): ?><a class="light-text" href="<?php echo get_theme_mod( 'facebook' ); ?>" target="_blank">Facebook</a><?php endif; ?> or <?php if ( get_theme_mod( 'instagram' ) ): ?><a class="light-text" href="<?php echo get_theme_mod( 'instagram' ); ?>" target="_blank">Instagram</a><?php endif; ?> for news, updates and other things.</h2>
                        </div>
                    </div>
                </div>

            </header>

            <div id="siteContent" class="site-content">