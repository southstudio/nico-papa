<?php

    //Custom post types
    //////News
    add_action( 'init', 'cpt_work' );
    function cpt_work() {
        $labels = array(
            'name'               => __( 'Portfolio' ),
            'singular_name'      => __( 'Portfolio' ),
            'menu_name'          => __( 'Portfolio' ),
            'all_items'          => __( 'Todos los trabajos' ),
            'insert_into_item'   => __( 'Insertar en trabajo' ),
            'add_new'            => __( 'Añadir nuevo trabajo' ),
            'add_new_item'       => __( 'Añadir nuevo trabajo' )
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'excerpt' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-layout',
            'rewrite'            => array( 'slug' => 'portfolio' ),
            'taxonomies'         => array( 'category' ),
            'register_meta_box_cb' => 'add_portfolio_metabox',
        );
        register_post_type( 'papa_portfolio', $args );
    };

    //////Blog
    add_action( 'init', 'cpt_blog' );
    function cpt_blog() {
        $labels = array(
            'name'               => __( 'Blog' ),
            'singular_name'      => __( 'Blog' ),
            'menu_name'          => __( 'Blog' ),
            'all_items'          => __( 'Todos los artículos' ),
            'insert_into_item'   => __( 'Insertar en artículo' ),
            'add_new'            => __( 'Añadir nuevo artículo' ),
            'add_new_item'       => __( 'Añadir nuevo artículo' )
        );
        $args = array(
            'labels'             => $labels,
            'supports'           => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'excerpt' ),
            'public'             => true,
            'has_archive'        => true,
            'menu_position'      => 5,
            'menu_icon'          => 'dashicons-edit',
            'rewrite'            => array( 'slug' => 'blog' ),
            'taxonomies'         => array( 'category' ),
        );
        register_post_type( 'papa_blog', $args );
    };

?>