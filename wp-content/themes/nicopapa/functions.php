<?php

    //Enqueue assets
    add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
    function my_theme_enqueue_styles() {

        wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/vendor/css/bootstrap.css' );
        wp_enqueue_style( 'fontawesome', get_stylesheet_directory_uri() . '/vendor/css/fontawesome.min.css' );
        wp_enqueue_style( 'font-sans', 'https://fonts.googleapis.com/css?family=PT+Sans:400,400i,700' );
        wp_enqueue_style( 'font-serif', 'https://fonts.googleapis.com/css?family=PT+Serif:400,700' );
        wp_enqueue_style( 'theme-info', get_stylesheet_directory_uri() . '/style.css' );
        wp_enqueue_style( 'theme-style', get_stylesheet_directory_uri() . '/assets/nicopapa.css' );
        wp_enqueue_script( 'jquery','https://code.jquery.com/jquery-3.3.1.slim.min.js', array( 'jquery' ),'',true );
        wp_enqueue_script( 'popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js', array( 'jquery' ),'',true );
        wp_enqueue_script('plugins-js', get_stylesheet_directory_uri() . '/vendor/js/app.min.js', array(), '', true );
        wp_enqueue_script('theme-js', get_template_directory_uri() . '/vendor/js/theme.js', array( 'jquery' ), '', true );

    }

    //Add feature image support
    add_theme_support( 'post-thumbnails' );

    //Menus
    function register_menu() {
        register_nav_menu('primary',__( 'Primary' ));
        register_nav_menu('footer',__( 'Footer' ));
    }
    add_action( 'init', 'register_menu' );

    //Remove website field from comments
    function remove_website_field($arg) {
        $arg['url'] = '';
        return $arg;
    }
    add_filter('comment_form_default_fields', 'remove_website_field');

    //Custom post types
    require get_template_directory() . '/inc/post-types.php';

    //Customizer addons
    require get_template_directory() . '/inc/customizer.php';

    //Custom meta boxes
    //require get_template_directory() . '/inc/post-meta.php';

    //Shorter excerpts for each post type
    function custom_excerpt_length($length) {
        global $post;
        if ($post->post_type == 'papa_portfolio')
            return 15;//////Portfolio
        else if ($post->post_type == 'papa_blog')
            return 10;//////Blog
        else
            return 15;
    }
    add_filter('excerpt_length', 'custom_excerpt_length');

    //No pagination on portfolio archive
    function portfolio_posts_per_page( $query ) {
        if ( !is_admin() && $query->is_main_query() && is_post_type_archive( 'papa_portfolio' ) ) {
            $query->set( 'posts_per_page', '20' );
        }
    }
    add_action( 'pre_get_posts', 'portfolio_posts_per_page' );

    //Redirect to home when trying to enter a portfolio detail
    function redirect_single_portfolio() {
        $queried_post_type = get_query_var('post_type');
        if ( is_single() && 'papa_portfolio' ==  $queried_post_type ) {
            wp_redirect( home_url(), 301 );
            exit;
        }
    }
    add_action( 'template_redirect', 'redirect_single_portfolio' );

    //Add more posts on scroll
    function more_post_ajax(){
        $offset = $_POST["offset"];
        $ppp = $_POST["ppp"];
    
        header("Content-Type: text/html");
    
        $args = array(
            'suppress_filters'  => true,
            'post_type'         => 'papa_portfolio',
            'posts_per_page'    => $ppp,
            'offset'            => $offset
        );
    
        $functionsLoop = new WP_Query($args);
    
        if ( $functionsLoop->have_posts() ) : while ( $functionsLoop->have_posts() ) : $functionsLoop->the_post();?>
        
            <script>
                var ids = [];
                var singleWork = document.querySelectorAll('.single-work-column');
                singleWork.forEach(work => {
                    var workID = work.getAttribute('id');
                    if (ids.indexOf(workID) === -1) {
                        ids.push(workID);
                        var foo = '<div class="column scroll-column invisible single-work-column" id="<?php the_ID(); ?>" data-id="<?php the_ID(); ?>">' +
                            '<a data-toggle="modal" data-target="#workModal">' +
                                '<?php the_post_thumbnail(); ?>' +
                            '</a>' +
                        '</div>';
                    }
                });
            </script>
    
            
            <div class="column scroll-column invisible single-work-column" id="<?php the_ID(); ?>" data-id="<?php the_ID(); ?>">
                <a data-toggle="modal" data-target="#workModal">
                    <?php the_post_thumbnail(); ?>
                </a>
            </div>
            
        <?php
            endwhile;
            endif;
            wp_reset_query();
    }
    add_action('wp_ajax_nopriv_more_post_ajax', 'more_post_ajax');
    add_action('wp_ajax_more_post_ajax', 'more_post_ajax');

    //Show post content on modal
    function fetch_modal_content() {
        if ( isset($_REQUEST) ) {
        $post_id = $_REQUEST['id'];
        global $post;
        $content_post = get_post($post_id);
        
        $post = get_post($post_id);
        $next_post = get_next_post();
        $prev_post = get_previous_post();
        $content = $content_post->post_content;
        $content = apply_filters('the_content', $content);
        $content = str_replace(']]>', ']]&gt;', $content);
        
        ?>
      
        <div id="loadedModal" class="loaded-modal">

            <!--<a href="#" class="close-modal" data-dismiss="modal" aria-label="Close">
                <i class="fas fa-times light-text" data-dismiss="modal" aria-label="Close"></i>
            </a>-->
            
            <?php echo '<img src="http://nicopapafoto.com/wp-content/uploads/2019/06/NP_Loader_02.gif" class="loading">';?>
            
            <a class="close-modal">
                <i class="fas fa-times light-text"></i>
            </a>
            <div id="workCarousel" class="carousel slide carousel-fade with-content" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item carousel-item-<?php echo get_the_ID($post_id); ?> d-flex flex-column justify-content-center active">
                        <h2 class="titulo light-text bold-text mb-20"><?php echo get_the_title($post_id); ?></h2>
                        <div class="portfolio-modal-image is-thumbnail d-flex">
                            <?php
                                $meta = get_post_meta($post_id, "Modal image", true);
                                if ($meta){
                                    echo '<img src="'.$meta.'">';
                                }
                                else{
                                    echo get_the_post_thumbnail($post_id);
                                }
                            ?>
                        </div>
                        <h3 class="pie light-text mt-20"><?php echo $content; ?></h3>
                        <script>
                            //Addons to slider
                            jQuery('#workCarousel .carousel-item h3 a').addClass('light-text');
                            var arrowRight = '<i class="fas fa-long-arrow-alt-right"></i>';
                            jQuery('#workCarousel .carousel-item h3 a').append(arrowRight);
                        </script>
                    </div>
                </div>
                <?php if ($next_post->ID) : ?>
                    <a class="carousel-control-prev" id="prev" data-id="<?php echo $next_post->ID; ?>" role="button">
                        <i class="fas fa-long-arrow-alt-left light-text"></i>
                    </a>
                <?php endif; ?>
                <?php if ($prev_post->ID) : ?>
                    <a class="carousel-control-next" id="next" data-id="<?php echo $prev_post->ID; ?>" role="button">
                        <i class="fas fa-long-arrow-alt-right light-text"></i>
                    </a>
                <?php endif; ?>
            </div>
        
        </div>

            <script>

                //Hide the modal on ESC keydown, and then empty it
                document.onkeydown = function(evt) {
                    evt = evt || window.event;
                    if (evt.keyCode == 27) {
                        jQuery('#workModal').modal('hide');
                        jQuery('#modal_target').html('');
                    }
                };

                //Hide the modal, and then empty it
                jQuery('.close-modal').click(function(){
                    jQuery('#workModal').modal('hide');
                    jQuery('#modal_target').html('');
                });
            
                //Show previous and next posts with the modal opened
                function changeModal(){
                    var $prevArrow = jQuery('#prev');
                    var $nextArrow = jQuery('#next');
                    var $modal_target = jQuery('#loadedModal');

                    ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

                    $prevArrow.click(function() {

                        var id = jQuery(this).data('id');

                        jQuery.ajax({
                            url: ajaxurl,
                            data: {
                                'action' : 'fetch_modal_content',
                                'id' : id
                            },
                            success:function(data) {
                                $modal_target.html('');
                                $modal_target.html(data);
                                //$modal.modal('show');
                            }
                        });

                    });
                    $nextArrow.click(function() {

                        var id = jQuery(this).data('id');

                        jQuery.ajax({
                            url: ajaxurl,
                            data: {
                                'action' : 'fetch_modal_content',
                                'id' : id
                            },
                            success:function(data) {
                                $modal_target.html('');
                                $modal_target.html(data);
                                //$modal.modal('show');
                            }
                        });

                    });
                    
                }
                changeModal();
            
            </script>
      
        <?php
        }
        die();
    }
    add_action( 'wp_ajax_fetch_modal_content', 'fetch_modal_content' );
    add_action( 'wp_ajax_nopriv_fetch_modal_content', 'fetch_modal_content' );

?>