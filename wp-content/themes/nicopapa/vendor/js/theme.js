jQuery(document).ready(function($){
    responsive = $(window).width() < 992;
    desktop = $(window).width() > 991;
    windowWidth = $(window).width();
    windowHeight = $(window).height();
    url = 'http://thesouthstudio.com/nicopapatest';

    $('.portfolio-grid').addClass('invisible');

    //Menu
    $(function(){
        $('.site-navbar .menu-item a').addClass('light-text bold-text');
    });

    //Responsive menu
    $('#menuNav').click(function(){
        $(this).toggleClass('open');
        $('body').toggleClass('menu-open');
        if($('body').hasClass('menu-open')){
            $('body, #page, .home-header').css({ overflow: 'hidden' });
        }
        else{
            $('body, #page, .home-header').css({ overflow: 'visible' });
        }
    });

    //Back to top
    $("#backToTop").click(function() {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

    //Lazy load
    $(function() {
        $('.portfolio-grid .column img').Lazy({
            scrollDirection: 'vertical',
            effect: 'fadeIn',
            onError: function(element) {
                console.log('error loading ' + element.data('src'));
            }
        });
    });

    //Disable work carousel autoplay
    $('#workCarousel').carousel({ interval: false });

    //Portfolio detail slider
    /*$(".portfolio-grid .column").each(function(){
        $(this).on("click", function(){
            $('#workCarousel .carousel-inner .carousel-item').removeClass('active');
            var columnTitle = $(this).text();
            var columnId = $(this).attr('id');
            var columnImage = $(this).find('img');
            var columnImageUrl = columnImage.attr('src');
            slideId = '#workCarousel .carousel-inner .carousel-item-' + columnId;
            //$('#workCarousel').carousel('pause');
            $(slideId).addClass('active');//Add class 'active' to the carousel-item-ID fired
            $('#workTitle').text(columnTitle);//Add work title to the #workTitle on carousel
            $('#workCarousel .carousel-item h3 a').addClass('light-text');
        });
    });*/

    //Portfolio detail slider addons
    $(function(){
        $('#workCarousel .carousel-item h3 a').addClass('light-text');
        var arrowRight = '<i class="fas fa-long-arrow-alt-right"></i>';
        $('#workCarousel .carousel-item h3 a').append(arrowRight);
    });
    //More addons to portfolio detail slider
    $('#workCarousel .carousel-item').each(function(){
        var portfolioImageContainer = $(this).find('.is-thumbnail');
        var portfolioImage = $(this).find('img');
        $(portfolioImageContainer).append(portfolioImage);
        $(portfolioImage).addClass('img-fluid');
    });

    $('.portfolio-grid .column a').click(function(){
        $('body').addClass('disable-scrolling');
    });
    $('.work-modal .modal-content .close-modal').click(function(){
        $('.prueba').removeClass('disable-scrolling');
    });
    
    //Current date on footer
    $(function(){
        var dateObject = new Date();
        var currentDate = dateObject.getFullYear();
        jQuery('#currentDate').text(currentDate);
    });

    //Prevent scroll of body when modal is opened
    /*if ($('html').hasClass('touchevents')) {
        //if ($('body').hasClass('modal-open')) {
        $('.portfolio-grid .column a').click(function(){
            document.ontouchmove = function(event){
                event.preventDefault();
            }
        });
        $('#workModal .close-modal').click(function(){
            document.ontouchmove = function(e){ return true; }
        })
    };*/

    $('.portfolio-grid').masonry({
        itemSelector: '.column',
        transitionDuration: 0
        //horizontalOrder: true
    });
    
});

//Fire Masonry and remove "invisible" class on images after the page loads
jQuery(window).on("load", function() {
    jQuery('.portfolio-grid').masonry({
        itemSelector: '.column',
        transitionDuration: 0
        //horizontalOrder: true
    });

    jQuery('.portfolio-grid').masonry('reloadItems');

    jQuery('.portfolio-grid').removeClass('invisible');
});

//While de page loads, display loading
jQuery(window).bind("load", function () {
    jQuery('#pageLoading').fadeOut(100);

    jQuery('.portfolio-grid').masonry({
        itemSelector: '.column',
        transitionDuration: 0
        //horizontalOrder: true
    });
});

jQuery('#workCarousel .carousel-item img').load(function(){
    jQuery('#workCarousel .carousel-item').each(function(){
        var titulo = jQuery(this).find('.titulo');
        var pie = jQuery(this).find('.pie');
        var img = jQuery(this).find('img');
        var imgHeight = img.height();
        var altoTitulo = titulo.height();
        var altoPie = pie.height();
        var windowHeight = jQuery(window).height() - altoTitulo - altoPie - 60;
        
    });
});