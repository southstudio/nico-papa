<?php get_header(); ?>

    <div class="container">
        <div class="row">
            <div class="col-12 text-center">
                <h1 class="light-text bold-text mb-10">Ups!</h1>
                <h2 class="light-text mb-20">Esta página no pudo ser encontrada</h2>
                <a href="javascript:history.back()" class="light-text bold-text">Volver</a>
            </div>
        </div>
    </div>

<?php get_footer(); ?>