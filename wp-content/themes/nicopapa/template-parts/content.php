<!-- Single blog post content -->
<?php $blogSinglePostThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

<div class="intern-cover blog-cover d-flex align-items-center justify-content-center" style="background-image:url(<?php echo $blogSinglePostThumbnail[0]?>);">
    <div class="overlay"></div>
    <h1 class="light-text bold-text"><?php the_title(); ?></h1>
</div>

<!-- HEADER HIGHLIGHT TEXT -->
<?php if (has_excerpt( $post->ID )) : ?>
    <div class="container">
        <div class="header-content">
            <div class="row">
                <div class="col-12 col-md-10 col-lg-8 mx-auto text-center light-text serif-text">
                    <?php the_excerpt(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="blog-post-content">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10 col-lg-8 mx-auto">
                <?php
                    the_content();
                    $post_material = get_post_custom_values( 'Material' );
                    if ($post_material) :
                        echo '<div class="materials mt-40">';
                        foreach ( $post_material as $key => $value ) {
                            echo '<div class="material d-flex align-items-center"><i class="fas fa-long-arrow-alt-right dark-text"></i><p>'.$value.'"</p></div>';
                        }
                        echo '</div>';
                    endif;
                ?>
                <?php
                    $fname = get_the_author_meta('first_name');
                    $lname = get_the_author_meta('last_name');
                    $full_name = '';
                    if( empty($fname)){
                        $full_name = $lname;
                    } elseif( empty( $lname )){
                        $full_name = $fname;
                    } else {
                        //both first name and last name are present
                        $full_name = "{$fname} {$lname}";
                    }
                ?>
                <p class="dark-text mt-50 mb-0 author-date">By <?php echo $full_name; ?>, on <?php echo get_the_date('d/m/Y'); ?></p>
            </div>
        </div>
    </div>
</div>