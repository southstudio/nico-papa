<!-- Single page content -->
<?php $blogSinglePostThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>

<div class="intern-cover about-cover d-flex align-items-center justify-content-center">
    <div class="overlay"></div>
    <h1 class="light-text bold-text">Santiago Nicolás Papa</h1>
</div>

<div class="container npf-page-template">
    <div class="row">
        <div class="col-12 col-md-10 col-lg-8 mx-auto">
            <div class="row">
                <div class="col-12 light-text">

					<?php
						the_content();
						$post_material = get_post_custom_values( 'Material' );
						if ($post_material) :
							echo '<div class="materials mt-40">';
							foreach ( $post_material as $key => $value ) {
								echo '<div class="material d-flex align-items-center"><i class="fas fa-long-arrow-alt-right light-text"></i><p>'.$value.'</p></div>';
							}
							echo '</div>';
						endif;
					?>

					<!-- Edit page -->
					<?php if ( get_edit_post_link() ) : ?>
						<div class="mt-50">
							<?php
								edit_post_link(
									sprintf(
										/* translators: %s: Name of current post */
										esc_html__( 'Edit %s' ),
										the_title( '"', '"', false )
									),
									'<div class="edit-link mt-50 light-text bold-text sans-text">',
									'</div>'
								);
							?>
						</div>
					<?php endif; ?>
				</div>
            </div>
        </div>
    </div>
</div>