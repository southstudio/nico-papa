<?php
/**
* Template Name: About Page
 */

get_header(); ?>

<div class="intern-cover about-cover d-flex align-items-center justify-content-center">
    <div class="overlay"></div>
    <h1 class="light-text bold-text">Santiago Nicolás Papa</h1>
</div>

<div class="container">
    <div class="row">
        <div class="col-12 col-md-10 col-lg-8 mx-auto">
            <div class="row">
                <div class="col-12 text-center">
                    <h3 class="light-text bold-text mb-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                    <p class="light-text serif-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper posuere ante, a vehicula lectus elementum sollicitudin. Curabitur fringilla magna urna, eget mollis tortor egestas eu. Donec pellentesque turpis sed convallis luctus. Curabitur vel facilisis sapien.</p>
                </div>
                <div class="col-12">
                    <h3 class="light-text bold-text mt-50 mb-20">Lorem ipsum dolor sit amet, consectetur adipiscing elit</h3>
                    <p class="light-text serif-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras semper posuere ante, a vehicula lectus elementum sollicitudin. Sed in ultricies nulla. Pellentesque vitae quam a lacus fringilla venenatis. Ut porttitor nulla sit amet gravida vestibulum. In neque eros, bibendum sit amet mattis id, blandit vehicula eros. Cras pulvinar dolor at odio scelerisque auctor. Curabitur fringilla magna urna, eget mollis tortor egestas eu. Donec pellentesque turpis sed convallis luctus. Curabitur vel facilisis sapien. Nulla odio quam, maximus eu venenatis id, auctor vitae libero. Cras enim sapien, sodales vel pulvinar in, elementum id quam. Praesent pellentesque consequat elit consequat tincidunt.</p>
                    <?php echo '<img src="'.$GLOBALS['global_url'].'/wp-content/uploads/2019/03/about2.png" class="mt-40 mb-40 mx-auto d-flex img-fluid">';?>
                    <p class="light-text serif-text"> Curabitur fringilla magna urna, eget mollis tortor egestas eu. Donec pellentesque turpis sed convallis luctus. Curabitur vel facilisis sapien. Nulla odio quam, maximus eu venenatis id, auctor vitae libero. Cras enim sapien, sodales vel pulvinar in, elementum id quam. Praesent pellentesque consequat elit consequat tincidunt.  Curabitur fringilla magna urna, eget mollis tortor egestas eu. Donec pellentesque turpis sed convallis luctus. Curabitur vel facilisis sapien.</p>
                </div>
                <div class="col-12">
                    <div class="materials mt-40">
                        <div class="material d-flex align-items-center">
                            <i class="fas fa-long-arrow-alt-right light-text"></i><p class="light-text serif-text">Canon 6D</p>
                        </div>
                        <div class="material d-flex align-items-center">
                            <i class="fas fa-long-arrow-alt-right light-text"></i><p class="light-text serif-text">Canon 5D Mark III</p>
                        </div>
                        <div class="material d-flex align-items-center">
                            <i class="fas fa-long-arrow-alt-right light-text"></i><p class="light-text serif-text">Sigma sd Quattro H</p>
                        </div>
                        <div class="material d-flex align-items-center">
                            <i class="fas fa-long-arrow-alt-right light-text"></i><p class="light-text serif-text">Canon 6D</p>
                        </div>
                        <div class="material d-flex align-items-center">
                            <i class="fas fa-long-arrow-alt-right light-text"></i><p class="light-text serif-text">Canon 5D Mark III</p>
                        </div>
                        <div class="material d-flex align-items-center">
                            <i class="fas fa-long-arrow-alt-right light-text"></i><p class="light-text serif-text">Sigma sd Quattro H</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer(); ?>