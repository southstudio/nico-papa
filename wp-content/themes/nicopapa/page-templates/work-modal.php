<!-- WORK MODAL -->
<div class="modal custom-fade" id="workModal" tabindex="-1" role="dialog" aria-labelledby="workModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg work-modal" role="document">
        <div class="modal-content"><!-- .d-flex -->
            <!--<div class="header d-flex align-items-center justify-content-between">
                <h2 id="workTitle" class="light-text bold-text"></h2>
                <i class="fas fa-times close-modal light-text" data-dismiss="modal" aria-label="Close"></i>
            </div>-->
            <a href="#" class="close-modal" data-dismiss="modal" aria-label="Close">
                <i class="fas fa-times light-text" data-dismiss="modal" aria-label="Close"></i>
            </a>
            <div id="workCarousel" class="carousel slide carousel-fade with-content" data-ride="carousel">
                <div class="carousel-inner">
                    <?php
                        $carouselArgs = array( 'post_type' => 'papa_portfolio', 'posts_per_page' => -1 );
                        $carouselLoop = new WP_Query( $carouselArgs );
                        while ( $carouselLoop->have_posts() ) : $carouselLoop->the_post();
                    ?>
                        <div class="carousel-item carousel-item-<?php the_ID(); ?> d-flex flex-column justify-content-center">
                            <h2 class="titulo light-text bold-text mb-20"><?php the_title() ?></h2>
                            <div class="portfolio-modal-image is-thumbnail d-flex">
                                <?php
                                    $modal_image = get_post_custom_values( 'Modal image' );
                                    if ($modal_image){
                                        foreach ( $modal_image as $key => $value ) {
                                            echo '<img src="'.$value.'">';
                                        }
                                    }
                                    else{
                                        echo the_post_thumbnail();
                                    }
                                ?>
                            </div>
                            <h3 class="pie light-text mt-20"><?php the_content(); ?></h3>
                        </div>
                    <?php endwhile ?>
                </div>
                <a class="carousel-control-prev" href="#workCarousel" role="button" data-slide="prev">
                    <i class="fas fa-long-arrow-alt-left"></i>
                </a>
                <a class="carousel-control-next" href="#workCarousel" role="button" data-slide="next">
                    <i class="fas fa-long-arrow-alt-right"></i>
                </a>
            </div>
        </div>
    </div>
</div>