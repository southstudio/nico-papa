<?php get_header(); ?>

    <?php if ( have_posts() ) : ?>

        <div class="grid-wrapper">
            <div class="portfolio-grid grid">
                <?php while ( have_posts() ) : the_post(); ?>
                
                    <div class="column" id="<?php the_ID(); ?>" data-id="<?php echo $post->ID; ?>">
                        <a data-toggle="modal" data-target="#workModal">
                            <?php the_post_thumbnail(); ?>
                            <!--<h3 class="light-text bold-text mt-10"><?php the_title();?></h3>-->
                        </a>
                    </div>
                    <div id="totalPosts" style="display: none;"><?php echo $totalPosts; ?></div>
                <?php
                    endwhile;
                    wp_reset_postdata();
                ?>
            </div>
            <div id="more_posts" class="load-more-posts text-center">
                <h3 class="light-text bold-text mb-10">Keep scrolling</h3>
                <i class="fas fa-long-arrow-alt-down light-text keep-scrolling"></i>
            </div>
        </div>
    
    <?php
        else ://If page doesn't have results
    ?>

        <div class="container no-results-alert">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="light-text bold-text mb-10">Ups!</h1>
                    <h2 class="light-text">There are no photos yet, but come back later!</h2>
                    <!--<a href="javascript:history.back()" class="light-text bold-text">Volver</a>-->
                </div>
            </div>
        </div>

    <?php endif; ?>

    <div class="modal custom-fade" id="workModal" tabindex="-1" role="dialog" aria-labelledby="workModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg work-modal" role="document">
            <div class="modal-content" id="modal_target">
            
            </div>
        </div>
    </div>

    <script>

        function loadModal(){
            var $button = jQuery('.column');
            var $modal = jQuery('#modal');
            var $modal_target = jQuery('#modal_target');

            ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';

            $button.click(function() {

                var id = jQuery(this).data('id');

                jQuery.ajax({
                    url: ajaxurl,
                    data: {
                        'action' : 'fetch_modal_content',
                        'id' : id
                    },
                    success:function(data) {
                        $modal_target.html('');
                        $modal_target.html(data);
                        $modal.modal('show');
                    }
                });

            });
        }
        loadModal();
    
        //Prevent scroll of body when modal is opened
        (function () {
            var _overlay = document.getElementById('workModal');
            var _clientY = null; // remember Y position on touch start

            _overlay.addEventListener('touchstart', function (event) {
                if (event.targetTouches.length === 1) {
                    // detect single touch
                    _clientY = event.targetTouches[0].clientY;
                }
            }, false);

            _overlay.addEventListener('touchmove', function (event) {
                if (event.targetTouches.length === 1) {
                    // detect single touch
                    disableRubberBand(event);
                }
            }, false);

            function disableRubberBand(event) {
                var clientY = event.targetTouches[0].clientY - _clientY;

                if (_overlay.scrollTop === 0 && clientY > 0) {
                    // element is at the top of its scroll
                    event.preventDefault();
                    
                }

                if (isOverlayTotallyScrolled() && clientY < 0) {
                    //element is at the top of its scroll
                    event.preventDefault();
                }
            }

            function isOverlayTotallyScrolled() {
                return _overlay.scrollHeight - _overlay.scrollTop <= _overlay.clientHeight;
            }
        }());

        jQuery(document).ready( function($) {
            var ajaxUrl = "<?php echo admin_url('admin-ajax.php')?>";
            var page = 1;
            var ppp = 40;// Post per page
            var canBeLoaded = true;

            $(window).scroll(function() {
                if($(window).scrollTop() + $(window).height() >= $(document).height() && canBeLoaded == true) {

                    // Post method to ajax
                    /*$.post(ajaxUrl, {
                        action: "more_post_ajax",
                        offset: (page * ppp),
                        ppp: ppp
                    })

                    .success(function(posts) {
                        $(".portfolio-grid").append(posts);
                        $("#more_posts").hide();
                        setTimeout(() => {
                            $('.portfolio-grid').masonry('reloadItems');
                        }, 100);
                        $('.portfolio-grid').masonry({
                            itemSelector: '.column',
                            transitionDuration: 0
                            //horizontalOrder: true
                        });
                        loadModal();
                        page++;
                        if (!$(posts).length) {
                            $("#more_posts").hide();
                        }else {
                            $("#more_posts").show();
                        }
                    });*/

                    var data = {
                        action: "more_post_ajax",
                        offset: (page * ppp),
                        ppp: ppp
                    };
                    
                    $.ajax({
                        url: ajaxUrl,
                        data: data,
                        type: 'POST',
                        beforeSend: function() {
                            canBeLoaded = false;
                        },
                        success: function(data) {
                            $(".portfolio-grid").append(data);
                            canBeLoaded = true;
                            page++;
                            $("#more_posts").hide();
                            setTimeout(() => {
                                $('.portfolio-grid').masonry('reloadItems');
                            }, 100);
                            $('.portfolio-grid').masonry({
                                itemSelector: '.column',
                                transitionDuration: 0
                                //horizontalOrder: true
                            });
                            loadModal();
                            if (!$(data).length) {
                                $("#more_posts").hide();
                            }else {
                                $("#more_posts").show();
                            }
                        },
                        complete: function() {
                            var totalJobs = jQuery('#totalPosts').text();
                            var visiblePosts = jQuery('.column:visible').length;
                            if (visiblePosts == totalJobs) {
                                canBeLoaded = false;
                            }
                        },
                        dataType: 'html'
                    });
                }

                var totalJobs = jQuery('#totalPosts').text();
                var visiblePosts = jQuery('.column:visible').length;
                if (visiblePosts == totalJobs) {
                    $("#more_posts").hide();
                }

                if ($(window).scrollTop() > 1) {
                    $('.portfolio-grid').masonry('reloadItems');
                    $('.portfolio-grid').masonry({
                        itemSelector: '.column',
                        transitionDuration: 0
                        //horizontalOrder: true
                    });
                    $(".portfolio-grid .scroll-column").removeClass('invisible');
                }
            });

        });

    </script>

<?php get_footer(); ?>