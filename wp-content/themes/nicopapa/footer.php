        </div><!-- /.site-content -->
        
        <footer>
            <div class="container-fluid">
                <div class="footer d-flex align-items-center justify-content-between">

                    <div class="footer-menu d-flex align-items-center">
                        <!-- FOOTER LOGO -->
                        <?php if ( get_theme_mod( 'footer_logo' ) ): ?>
                            <a href="<?php echo esc_url( home_url( '/' )); ?>">
                                <img src="<?php echo esc_attr(get_theme_mod( 'footer_logo' )); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>">
                            </a>
                        <?php else : ?>
                            <a class="site-title" href="<?php echo esc_url( home_url( '/' )); ?>"><?php esc_url(bloginfo('name')); ?></a>
                        <?php endif; ?>

                        <!-- FOOTER MENU -->
                        <?php
                            wp_nav_menu(array(
                                'theme_location'    => 'footer',
                                'container'       => 'div',
                                'container_id'    => 'footerNav',
                                'container_class' => 'footer-nav',
                                'menu_class'      => 'menu-footer',
                            ));
                        ?>
                    </div>

                    <div class="social-media d-flex align-items-center">
                        <div class="site-social-media d-flex align-items-center">
                            <!-- SOCIAL MEDIA -->
                            <?php if ( get_theme_mod( 'twitter' ) ): ?>
                                <a class="light-text" href="<?php echo get_theme_mod( 'twitter' ); ?>" target="_blank">
                                    <i class="fab fa-twitter"></i></i>
                                </a>
                            <?php endif; ?>
                            <?php if ( get_theme_mod( 'behance' ) ): ?>
                                <a class="light-text" href="<?php echo get_theme_mod( 'behance' ); ?>" target="_blank">
                                    <i class="fab fa-behance"></i></i>
                                </a>
                            <?php endif; ?>
                            <?php if ( get_theme_mod( 'dribbble' ) ): ?>
                                <a class="light-text" href="<?php echo get_theme_mod( 'dribbble' ); ?>" target="_blank">
                                    <i class="fab fa-dribbble"></i></i>
                                </a>
                            <?php endif; ?>
                            <?php if ( get_theme_mod( 'youtube' ) ): ?>
                                <a class="light-text" href="<?php echo get_theme_mod( 'youtube' ); ?>" target="_blank">
                                    <i class="fab fa-youtube"></i></i>
                                </a>
                            <?php endif; ?>
                        </div>
                        <a href="#" id="backToTop" class="go-top light-text bold-text">
                            <i class="fas fa-long-arrow-alt-up light-text"></i>
                        </a>
                    </div>

                </div>
            </div>
        </footer>

        <div class="copyright">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6 text-left">
                        <p class="light-text">©&nbsp;<span id="currentDate"></span>&nbsp;Nico Papa Foto</p>
                    </div>
                    <div class="col-6 text-right">
                        <p class="light-text">Site by <a href="http://south.studio" target="_blank" class="light-text">South Studio</a></p>
                    </div>
                </div>
            </div>
        </div>

        </div><!-- .site -->
        
        <?php wp_footer(); ?> 
    </body>
</html>