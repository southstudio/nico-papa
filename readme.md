## Future Lazy Load posibilities
### [Lazy Load](https://appelsiini.net/projects/lazyload/v1/)
```
<?php
    $thumbnailUrl = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
?>
<img class="lazy" data-original="<?php echo $thumbnailUrl[0] ?>">
```

## Theme styles
`sass --watch assets/scss/nicopapa.scss:assets/nicopapa.css --style compressed` to kickstart styles